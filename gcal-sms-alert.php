#!/usr/bin/env php
<?php
require __DIR__.'/vendor/autoload.php';

use Romain\GcalSmsAlert\Command;
use Symfony\Component\Console\Application;

$application = new Application();
$application->add(new Command\CalendarsListCommand());
$application->add(new Command\CalendarListCommand());
$application->run();