<?php

namespace Romain\GcalSmsAlert\Processor;

/**
 * Main interface that return a way to process events (ProcessorInterface)
 */
interface FactoryInterface {
	
	/**
	 * @return \Romain\StackProcessor\Processor\ProcessorInterface
	 */
	public function create();
}