<?php

namespace Romain\GcalSmsAlert\Processor\FreeMobile;

use Romain\GcalSmsAlert\Processor\FactoryInterface;
use Romain\GcalSmsAlert\Decorator;

/**
 * Use FreeMobileSmsClient and /conf/freemobile.json file for config
 */
class FileConfFreeMobileFactory implements FactoryInterface {

	public function create() {
		$file = dirname(__FILE__).'/../../../../conf/freemobile.json';

		if(!file_exists($file)) {
			throw new \Exception('Config file not found : '.$file);
		}

		$conf = json_decode(file_get_contents($file), true);

		return new FreeMobileSmsProcessor(
				$conf['user'],
				$conf['user_key'],
				new Decorator\Event\SimpleSmsAlert()
			);
	}
}
