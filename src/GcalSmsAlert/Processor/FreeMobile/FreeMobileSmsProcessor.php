<?php

namespace Romain\GcalSmsAlert\Processor\FreeMobile;

use Romain\StackProcessor\Processor\ProcessorInterface;
use Romain\GcalSmsAlert\Decorator\DecoratorInterface;

class FreeMobileSmsProcessor implements ProcessorInterface {

	/**
	 *
	 * @var DecoratorInterface
	 */
	protected $_decorator = null;

	/**
	 *
	 * @var FreeMobileSmsClient
	 */
	protected $_client = null;

	/**
	 *
	 * @param type $user
	 * @param type $user_key
	 * @param DecoratorInterface $decorator
	 */
	public function __construct($user, $user_key, \Romain\GcalSmsAlert\Decorator\Event\DecoratorInterface $decorator) {
		$this->_client = new FreeMobileSmsClient();
		$this->_client->initialize();
		$this->_client->add($user, $user_key);
		$this->_decorator = $decorator;
	}

	/**
	 *
	 * @param \Traversable $iterator
	 */
	public function processAll(\Traversable $iterator) {
		foreach($iterator as $event) {
			$this->processOne($event);
		}
	}

	/**
	 *
	 * @param type $event
	 */
	public function processOne($event) {
		if($event instanceof \Google_Service_Calendar_Event) {
			$this->_client->sendAll($this->_decorator->decorate($event));
		} else {
			throw new \Exception('Can only process \Google_Service_Calendar_Event , not '.  get_class($event));
		}
	}
}