<?php

namespace Romain\GcalSmsAlert\Processor\FreeMobile;

use Romain\GcalSmsAlert\Processor\FactoryInterface;
use Romain\GcalSmsAlert\Decorator;

class MyFreeMobileFactory implements FactoryInterface {

	public function create() {
		return new FreeMobileSmsProcessor(
				'17421075',
				'DRJQN18ETIzMzU',
				new Decorator\Event\SimpleSmsAlert()
			);
	}
}
