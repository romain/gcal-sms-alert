<?php

namespace Romain\GcalSmsAlert\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Romain\GcalSmsAlert\Gcal;

class CalendarsListCommand extends Command {

	protected function configure() {
		$this
			->setName('calendars:list')
			->setDescription('List available calendars')
			->addOption(
				'decorator', null, \Symfony\Component\Console\Input\InputOption::VALUE_OPTIONAL, 'Romain\GcalSmsAlert\Decorator\Calendar\DecoratorInterface classname', 'Romain\GcalSmsAlert\Decorator\Calendar\Simple'
			);
	}

	/**
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 */
	protected function execute(InputInterface $input, OutputInterface $output) {
		$output->writeln('Initalize Google API..');
		$gcal = Gcal::instance();
		$gcal->initialize();

		$output->writeln('done.');

		$results = $gcal->service()->calendarList->listCalendarList();
		foreach($results as $calendarListEntry) {
			$this->decorate($calendarListEntry, $input, $output);
		}
	}

	/**
	 *
	 * @param \Google_Service_Calendar_Event $calendarListEntry
	 */
	public function decorate(\Google_Service_Calendar_CalendarListEntry $calendarListEntry, InputInterface $input, OutputInterface $output) {
		$class = $input->getOption('decorator');
		$decorator = new $class();
		if($decorator instanceof \Romain\GcalSmsAlert\Decorator\Calendar\DecoratorInterface) {
			$output->writeln($decorator->decorate($calendarListEntry));
		} else {
			throw new \Exception('Can only decorate with Calendar\DecoratorInterface. Not with '.get_class($decorator));
		}
	}
}
