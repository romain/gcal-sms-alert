<?php

namespace Romain\GcalSmsAlert\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Romain\GcalSmsAlert\Gcal;

class CalendarsListCommand extends Command {

	protected function configure() {
		$this
			->setName('app:authorize')
			->setDescription('Authorize app to access google calendar account');
	}

	/**
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 */
	protected function execute(InputInterface $input, OutputInterface $output) {
		$output->writeln('Initalize Google API..');
		$gcal = Gcal::instance();

		$gcal->requestAuthorization();
	}
}
