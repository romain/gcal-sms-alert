<?php

namespace Romain\GcalSmsAlert\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Romain\GcalSmsAlert\Gcal;

class CalendarListCommand extends Command {

	protected function configure() {
		$this
			->setName('calendar:list')
			->setDescription('List next calendar events')
			->addArgument(
				'calendar', \Symfony\Component\Console\Input\InputArgument::OPTIONAL, 'calendar id', 'primary'
			)
			->addOption(
				'nb', null, \Symfony\Component\Console\Input\InputOption::VALUE_OPTIONAL, 'max number of events displayed', 20
			)
			->addOption(
				'filter', null, \Symfony\Component\Console\Input\InputOption::VALUE_OPTIONAL, 'FilterInterface classname (example : Romain\\GcalSmsAlert\\Filter\\EmailReminder )', null
			)
			->addOption(
				'decorator', null, \Symfony\Component\Console\Input\InputOption::VALUE_OPTIONAL, 'Romain\GcalSmsAlert\Decorator\Event\DecoratorInterface classname', 'Romain\GcalSmsAlert\Decorator\Event\VarDump'
			)
			->addOption(
				'processor', null, \Symfony\Component\Console\Input\InputOption::VALUE_OPTIONAL, 'Romain\GcalSmsAlert\Processor\ProcessorInterface classe name (example : Romain\\GcalSmsAlert\\Processor\\FreeMobile\\FileConfFreeMobileFactory)', null
			)
		;
	}

	/**
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 */
	protected function execute(InputInterface $input, OutputInterface $output) {
		$output->writeln('Initalize Google API..');
		$gcal = Gcal::instance();
		$gcal->initialize();
		$output->writeln('done.');
		$calendarId = $input->getArgument('calendar');

		$events = $this->events($gcal, $input);

		if (count($events) == 0) {
			$output->writeln('No events in '.$calendarId);
		} else {
			$output->writeln('Next '.count($events).' events in '.$calendarId.' :');
			// display
			foreach ($events as $event) {
				$this->decorate($event, $input, $output);
			}
			// process ?
			$this->process($events, $input);
		}
	}

	/**
	 *
	 * @param type $events
	 * @throws \Exception
	 */
	public function process($events, InputInterface $input) {

		// get factory
		$class = $input->getOption('processor');
		if(!$class) {
			return;
		}
		
		$factory = new $class();
		if(!$factory instanceof \Romain\GcalSmsAlert\Processor\FactoryInterface) {
			throw new \Exception('Can only create with Processor\FactoryInterface. Not with '.get_class($factory));
		}

		// process events
		$processor = $factory->create();
		$processor->processAll(new \ArrayIterator($events));
	}

	/**
	 * 
	 * @param \Google_Service_Calendar_Event $event
	 */
	public function decorate(\Google_Service_Calendar_Event $event, InputInterface $input, OutputInterface $output) {
		$class = $input->getOption('decorator');
		$decorator = new $class();
		if($decorator instanceof \Romain\GcalSmsAlert\Decorator\Event\DecoratorInterface) {
			$output->writeln($decorator->decorate($event));
		} else {
			throw new \Exception('Can only decorate with Event\DecoratorInterface. Not with '.get_class($decorator));
		}
	}

	/**
	 *
	 * @param Gcal $gcal
	 * @return type
	 */
	protected function events(Gcal $gcal, InputInterface $input) {
		$calendarId = $input->getArgument('calendar');
		$optParams = array(
			'maxResults' => $input->getOption('nb'),
			'orderBy' => 'startTime',
			'singleEvents' => TRUE,
			'timeMin' => date('c'),
		);
		$results = $gcal->service()->events->listEvents($calendarId, $optParams);
		$events = $results->getItems();
		$evs = array();
		foreach($events as $event) {
			if(empty($input->getOption('filter'))) {
				$evs[] = $event;
			} else {
				$class = $input->getOption('filter');
				$filter = new $class();
				if(!$filter instanceof \Romain\GcalSmsAlert\Filter\FilterInterface) {
					throw new \Exception('Can only filter with Filte\FilterInterface. Not with '.get_class($filter));
				}

				if($filter->alert($event)) {
					$evs[] = $event;
				}
			}
		}
		return $evs;
	}

}
