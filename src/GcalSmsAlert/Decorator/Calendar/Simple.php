<?php

namespace Romain\GcalSmsAlert\Decorator\Calendar;

/**
 * 
 */
class Simple implements DecoratorInterface {

	public function decorate(\Google_Service_Calendar_CalendarListEntry $calendarListEntry) {
		$text = $calendarListEntry->getSummary()." => ".$calendarListEntry->getId();
		return $text;
	}

}