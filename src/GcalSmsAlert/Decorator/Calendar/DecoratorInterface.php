<?php

namespace Romain\GcalSmsAlert\Decorator\Calendar;

/**
 * Event decorator interface
 */
interface DecoratorInterface {

	/**
	 * 
	 * @param \Google_Service_Calendar_Event $calendar
	 */
	public function decorate(\Google_Service_Calendar_CalendarListEntry $calendar);
}