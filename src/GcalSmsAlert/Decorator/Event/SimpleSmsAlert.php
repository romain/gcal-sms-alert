<?php

namespace Romain\GcalSmsAlert\Decorator\Event;

/**
 * 
 */
class SimpleSmsAlert implements DecoratorInterface {

	/**
	 *
	 * @param \Google_Service_Calendar_Event $event
	 * @return string
	 */
	public function decorate(\Google_Service_Calendar_Event $event) {
		$text = '';

		$start = $event->start->dateTime;
		if (empty($start)) {
			$start = $event->start->date;
		}
		$text .= $start.' : '.$event->getSummary().' ('.$event->organizer['displayName'].')';

		return $text;
	}
}