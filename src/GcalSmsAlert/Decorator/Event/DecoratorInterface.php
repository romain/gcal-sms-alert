<?php

namespace Romain\GcalSmsAlert\Decorator\Event;

/**
 * Event decorator interface
 */
interface DecoratorInterface {

	/**
	 * 
	 * @param \Google_Service_Calendar_Event $event
	 */
	public function decorate(\Google_Service_Calendar_Event $event);
}