<?php

namespace Romain\GcalSmsAlert\Decorator\Event;

/**
 * 
 */
class VarDump implements DecoratorInterface {

	/**
	 *
	 * @param \Google_Service_Calendar_Event $event
	 * @return string
	 */
	public function decorate(\Google_Service_Calendar_Event $event) {
		ob_clean();
		var_dump($event);
		return ob_get_clean();
	}
}