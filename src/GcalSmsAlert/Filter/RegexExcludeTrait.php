<?php

namespace Romain\GcalSmsAlert\Filter;

trait RegexExcludeTrait {

	/**
	 *
	 * @param \Google_Service_Calendar_Event $event
	 * @return type
	 */
	protected function _regex_exclude(\Google_Service_Calendar_Event $event) {
		if(empty($this->_options['exclude'])) {
			return false;
		}
		return preg_match($this->_options['exclude'], $event->getDescription());
	}
}