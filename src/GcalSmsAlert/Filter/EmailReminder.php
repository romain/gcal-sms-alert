<?php

namespace Romain\GcalSmsAlert\Filter;

/**
 *
 */
class EmailReminder implements FilterInterface {
	use RegexExcludeTrait;

	protected $_options = array(
		'exclude' => '',
		'delay' => '45'
	);

	/**
	 *
	 * @param array $options
	 */
	public function __construct(array $options = array()) {
		$this->_options = $options + $this->_options;
	}

	/**
	 * Alert if :
	 * - an email reminder si matching for this event
	 * - no "nosmsalert" tag is present in event description
	 * 
	 * @param \Google_Service_Calendar_Event $event
	 * @return boolean
	 */
	public function alert(\Google_Service_Calendar_Event $event) {

		if($this->_regex_exclude($event)) {
			return false;
		}

		$reminders = $event->getReminders();
		foreach($reminders as $reminder) {
			if($this->_match($event, $reminder)) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * Reminder match ?
	 * @param \Romain\GcalSmsAlert\Responsible\Google_Service_Calendar_EventReminder $reminder
	 */
	protected function _match(\Google_Service_Calendar_Event $event, \Google_Service_Calendar_EventReminder $reminder) {
		if($reminder->getMethod() != 'email') {
			return false;
		}
		
		if($reminder->getMinutes()) {
			return true;
		}
	}
}