<?php

namespace Romain\GcalSmsAlert\Filter;

/**
 *
 */
class Today implements FilterInterface {
	use RegexExcludeTrait;

	/**
	 *
	 * @var array
	 */
	protected $_options = array(
		'exclude' => '',
	);

	/**
	 *
	 * @param array $options
	 */
	public function __construct(array $options = array()) {
		$this->_options = $options + $this->_options;
	}

	/**
	 * Alert if :
	 * - an email reminder si matching for this event
	 * - no "nosmsalert" tag is present in event description
	 * 
	 * @param \Google_Service_Calendar_Event $event
	 * @return boolean
	 */
	public function alert(\Google_Service_Calendar_Event $event) {

		if($this->_regex_exclude($event)) {
			return false;
		}

		return $event->start->date === date('Y-m-d');
	}

}