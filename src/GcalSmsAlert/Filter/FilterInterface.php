<?php

namespace Romain\GcalSmsAlert\Filter;

/**
 * Event filter strategies to alert about an event
 */
interface FilterInterface {

	/**
	 * Should we alert NOW about this event ?
	 * @param \Google_Service_Calendar_Event $event
	 * @return boolean
	 */
	public function alert(\Google_Service_Calendar_Event $event);
}