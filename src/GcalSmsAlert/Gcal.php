<?php

namespace Romain\GcalSmsAlert;

/**
 *
 */
class Gcal {

	/**
	 *
	 * @var Gcal
	 */
	protected static $instance = null;

	/**
	 *
	 * @var type 
	 */
	protected $_options = array(
		'application_name' => 'Google Calendar SMS Alert',
		'credentials_path' => '~/.credentials/calendar-php-quickstart.json',
		'client_secret_path' => 'client_secret.json',
		'scopes' => null
	);

	/**
	 *
	 * @var \Google_Service_Calendar
	 */
	protected $_service = null;

	/**
	 *
	 * @var \Google_Client
	 */
	protected $_client = null;

	/**
	 *
	 * @param type $options
	 */
	public function __construct($options) {
		$options += $this->_options;
		if (empty($options['scopes'])) {
			$options['scopes'] = implode(' ', array(\Google_Service_Calendar::CALENDAR_READONLY));
		}
		if(!empty($options['credentials_path'])) {
			$options['credentials_path'] = $this->_expandHomeDirectory($options['credentials_path']);
		}
		$this->_options = $options;
	}

	/**
	 *
	 * @return Gcal
	 */
	public function instance(array $options = array()) {
		if (is_null(self::$instance)) {
			self::$instance = new Gcal($options);
		}
		return self::$instance;
	}

	/**
	 *
	 */
	public function initialize() {
		$this->_client = $this->_getClient();
		$this->_service = $this->_service();
	}

	/**
	 *
	 * @return \Google_Service_Calendar
	 */
	public function service() {
		return $this->_service();
	}

	public function events() {
		
	}

	public function calendars() {
		return $this->service()->calendarList->listCalendarList();
	}

	/**
	 *
	 * @return \Google_Service_Calendar
	 */
	protected function _service() {
		$service = new \Google_Service_Calendar($this->_client);
		return $service;
	}

	/**
	 * Returns an authorized API client.
	 * @return Google_Client the authorized client object
	 */
	protected function _getClient() {
		$client = new \Google_Client();
		$client->setApplicationName($this->_options['application_name']);
		$client->setScopes($this->_options['scopes']);
		$client->setAuthConfigFile($this->_options['client_secret_path']);
		$client->setAccessType('offline');

		// Load previously authorized credentials from a file.
		if (file_exists($this->_options['credentials_path'])) {
			$accessToken = file_get_contents($this->_options['credentials_path']);
		} else {
			throw new \Exception('Authorization credentials not yet requested (see requestAuthorization())');
		}
		$client->setAccessToken($accessToken);

		// Refresh the token if it's expired.
		if ($client->isAccessTokenExpired()) {
			$client->refreshToken($client->getRefreshToken());
			file_put_contents($this->_options['credentials_path'], $client->getAccessToken());
		}
		return $client;
	}

	/**
	 *
	 * @return \Google_Client
	 */
	public function requestAuthorization() {
		$client = new \Google_Client();
		$client->setApplicationName($this->_options['application_name']);
		$client->setScopes($this->_options['scopes']);
		$client->setAuthConfigFile($this->_options['client_secret_path']);
		$client->setAccessType('offline');

		if (file_exists($this->_options['credentials_path'])) {
			throw new \Exception('Authorization credentials allready requested');
		}
		
		// Request authorization from the user.
		$authUrl = $client->createAuthUrl();
		printf("Open the following link in your browser:\n%s\n", $authUrl);
		print 'Enter verification code: ';
		$authCode = trim(fgets(STDIN));

		// Exchange authorization code for an access token.
		$accessToken = $client->authenticate($authCode);

		// Store the credentials to disk.
		if (!file_exists(dirname($this->_options['credentials_path']))) {
			mkdir(dirname($this->_options['credentials_path']), 0700, true);
		}
		file_put_contents($this->_options['credentials_path'], $accessToken);
		printf("Credentials saved to %s\n", $this->_options['credentials_path']);

		$client->setAccessToken($accessToken);

		// Refresh the token if it's expired.
		if ($client->isAccessTokenExpired()) {
			$client->refreshToken($client->getRefreshToken());
			file_put_contents($this->_options['credentials_path'], $client->getAccessToken());
		}
		return $client;
	}

	/**
	 * Expands the home directory alias '~' to the full path.
	 * @param string $path the path to expand.
	 * @return string the expanded path.
	 */
	protected function _expandHomeDirectory($path) {
		$homeDirectory = getenv('HOME');
		if (empty($homeDirectory)) {
			$homeDirectory = getenv("HOMEDRIVE") . getenv("HOMEPATH");
		}
		return str_replace('~', realpath($homeDirectory), $path);
	}

}
